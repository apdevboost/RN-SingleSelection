import React, {Component} from 'react';
import {Text, View, FlatList, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';

import {get, orderBy} from 'lodash';

import Header from '../Header'
import Search from '../Search'
import RowItem from './RowItem'

class SingleSelection extends Component {

  static propTypes = {
    selectedItem: PropTypes.object,
    items: PropTypes.array.isRequired,
    uniqueKey: PropTypes.string,
    canAddItems: PropTypes.bool,
    onAddItem: PropTypes.func,
    onChangeInput: PropTypes.func,
    displayKey: PropTypes.string,
    onItemSelected: PropTypes.func.isRequired,
    onClosePress: PropTypes.func,
  };

  static defaultProps = {
    selectedItem: {},
    items: [],
    uniqueKey: 'id',
    onChangeInput: () => {},
    displayKey: 'name',
    canAddItems: false,
    onAddItem: () => {},
    onItemSelected: () => {},
    onClosePress: () => {},
  };

  constructor(props: PropsType) {
    super(props);
    this.state = {
      searchTerm: '',
    }
  }

  onSearchTextChange = (text) => {
    this.setState({ searchTerm: text });
  }

  isItemSelected = item => {
    const { uniqueKey, selectedItem } = this.props;
    return selectedItem[uniqueKey] === item[uniqueKey]? true : false
  };

  onRowItemClicked = item => {
    const {
      onItemSelected
    } = this.props;

    onItemSelected(item);
  };

  filterItems = searchTerm => {
    const { items, displayKey } = this.props;
    const filteredItems = [];
    items.forEach(item => {
      const parts = searchTerm.trim().split(/[ \-:]+/);
      const regex = new RegExp(`(${parts.join('|')})`, 'ig');
      if (regex.test(get(item, displayKey))) {
        filteredItems.push(item);
      }
    });
    return filteredItems;
  };

  _renderFlatList = (items) => {
    const {
      canAddItems,
      uniqueKey,
      selectedItem
    } = this.props;
    const { searchTerm } = this.state;
    let component = null;
    // If searchTerm matches an item in the list, we should not add a new
    // element to the list.
    let searchTermMatch;
    let itemList;
    let addItemRow;
    const renderItems = searchTerm ? this.getOrderItems(this.filterItems(searchTerm)) : items;
    if (renderItems.length) {
      itemList = (
        <FlatList
          data={renderItems}
          extraData={selectedItem}
          keyExtractor={item => item[uniqueKey]}
          renderItem={rowData => 
            <RowItem 
              item = {rowData.item}
              onPress = {this.onRowItemClicked}/>
          }/>
      );
      searchTermMatch = renderItems.filter(item => item.name === searchTerm)
        .length;
    } else if (!canAddItems) {
      itemList = (
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Text
            style={{
                flex: 1,
                marginTop: 20,
                textAlign: 'center',
                color: '#ff0000'}}>
            Cannot find the items
          </Text>
        </View>
      );
    }

    if (canAddItems && !searchTermMatch && searchTerm.length) {
      addItemRow = this._getRowNew({ name: searchTerm });
    }
    component = (
      <View>
        {itemList}
        {addItemRow}
      </View>
    );
    return component;
  };
  
  getOrderItems = (items) => {
    return orderBy(items, ['isSelected', 'name'], ['desc', 'asc'])
  }

  getSortedItemChecklist = (items) => {
    var self = this;

    items.forEach(function(item){item.isSelected =  self.isItemSelected(item)});
    return this.getOrderItems(this.props.items)
  }


  render() {

    return (
      <View style={styles.singleSelectionContainer}>
        <Header
          onClosePress={this.props.onClosePress}
          isConfirmEnable={false}/>
        <Search 
          onChangeText={this.onSearchTextChange}/>
        <View>
          {this._renderFlatList(this.getSortedItemChecklist(this.props.items))}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    singleSelectionContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white'
  },

});

export default SingleSelection;